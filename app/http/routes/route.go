package routes

/**
 * 配置路由
 */
import (
	"snow-im/app/http/controllers"
	"snow-im/app/http/middlewares"
	"snow-im/app/utils/metric"
	"snow-im/config"

	"github.com/gin-gonic/gin"
	"github.com/qit-team/snow-core/http/middleware"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// api路由配置
func RegisterRoute(router *gin.Engine) {
	//middleware: 服务错误处理 => 生成请求id => access log
	router.Use(middlewares.ServerRecovery(), middleware.GenRequestId, middleware.GenContextKit, middleware.AccessLog())

	if config.GetConf().PrometheusCollectEnable && config.IsEnvEqual(config.ProdEnv) {
		router.Use(middlewares.CollectMetric())
		metric.Init(metric.EnableRuntime(), metric.EnableProcess())
		metricHandler := metric.Handler()
		router.GET("/metrics", func(ctx *gin.Context) {
			metricHandler.ServeHTTP(ctx.Writer, ctx.Request)
		})
	}
	var api = router.Group("/api")
	api.POST("/goodsList", controllers.GoodsList)
	api.POST("/order/create", controllers.CreateOrder)
	api.POST("/order/record", controllers.OrderList)
	api.POST("/order/notify", controllers.OrderNotify)

	api.GET("/is_audit", controllers.AppCheck)
	//router.NoRoute(controllers.Error404)

	//api版本
	//v1 := router.Group("/v1")
	//{
	//	v1.GET("/banner_list", controllers.GetBannerList)
	//}

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
