package im

import (
	"github.com/go-netty/go-netty"
	"snow-im/app/utils"
	"snow-im/app/utils/netool"
)

var ImLogic ImHandler

func Init() {
	ImLogic = ImHandler{
		ImRouter,
	}
}

type ImHandler struct {
	netool.Router
}

//消息处理
func (this *ImHandler) HandMessage(message []byte, connect netty.InboundContext) error {
	var msgId, msg = utils.DecodeMessage(message)
	utils.Log(nil,"message",msgId,msg)
	err := this.Router.HandleMsg(msgId, msg, connect)
	return err
}
