package im

import (
	"fmt"
	"github.com/go-netty/go-netty"
	"snow-im/app/constants/msgid"
	"snow-im/app/utils/netool"
)

var ImRouter netool.Router

func init()  {
	ImRouter = netool.Router{}
	ImRouter.AddRouter(msgid.SINGLE_MSG,SendMsgtoUser)
	ImRouter.AddRouter(msgid.GROUP_MSG,SendMsgtoMultUser)
}
//验证
func Auth(msg []byte, connect netty.InboundContext) error{
	fmt.Println(string(msg))
	return nil
}
//单聊
func SendMsgtoUser(msg []byte, connect netty.InboundContext) error{
	fmt.Println(string(msg))
	return nil
}
//群聊
func SendMsgtoGroup(msg []byte, connect netty.InboundContext) error{
	return nil
}
//组聊
func SendMsgtoMultUser(msg []byte, connect netty.InboundContext) error{
	return nil
}


