package errorcode

const (
	//成功
	Success = 200
	//没有路由
	NoRoute = 201

	//参数错误
	ParamError = 400

	//未经授权
	NotAuth = 401

	//请求被禁止
	Forbidden = 403

	//找不到页面
	NotFound = 404

	//系统错误
	SystemError = 500
	//用户相关
	UserExist     = 1000
	PasswordError = 1001
)

var MsgEN = map[int]string{
	Success:        "Success",
	ParamError:     "Param Error",
	NotAuth:        "Not Authorized",
	Forbidden:      "Forbidden",
	NotFound:       "Not Found",
	SystemError:    "System Error",
	UserExist:      "User Exist",
	PasswordError:  "PasswordError",
	NoRoute:        "No Route",
}

var MsgMap map[string]map[int]string = map[string]map[int]string{"en": MsgEN}

func GetMsg(code int, local string) string {
	if local == "" {
		local = "en"
	}
	if msg, ok := MsgMap[local][code]; ok {
		return msg
	}
	return ""
}
