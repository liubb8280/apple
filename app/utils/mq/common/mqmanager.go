package common

import "snow-im/app/utils/mq"

var MqManager = CMqManager{}

type CMqManager struct {
	mqs map[string]Imq
}

func (this *CMqManager) InitMq() {
	this.mqs = make(map[string]Imq)
	this.mqs["rabbitmq"] = rabbitmq.RabbitMq{}
	this.mqs["nsq"] = rabbitmq.NsqMq{}
	this.mqs["kafka"] = rabbitmq.KafkaMq{}
}
func (this *CMqManager) GetMqByName(name string) Imq {
	return this.mqs[name]
}
