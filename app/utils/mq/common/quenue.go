package common

import (
	"github.com/streadway/amqp"
	utils2 "snow-im/app/utils"
	"snow-im/app/utils/mq"
	"snow-im/config"
)

const (
//ACCOUNT_CHANGE               = "account_change"
//PAYOUT                       = "payout"
//PayMerchantNotify            = "pay_mechant_notify"
//PayMerchantNotifyNow         = "pay_mechant_notify_now"
//PayMerchantNotifyNowExchange = "pay_mechant_notify_now_Exchange"
//PayMerchantExchange          = "pay_mechant_exchange"
//PaymentQueryQueue            = "payment_query_queue"
//PaymentQueryExchange         = "payment_query_exchange"
//TrxQueryExchange             = "trx_query_exchange"
//TrxQueryPayQunue                = "trx_query_pay_Qunue"
//TrxQueryPayoutQunue                = "trx_query_payout_Qunue"
//RabbitMqQunue = "rabbitmq"
//NsqMqQunue = "nsq"
//TrxOrderHandle = "trx_order_handle"
//SubAccountForzen = "sub_account_frozen"
)

func init() {
	//初始化工作队列
	//utils.JobQueue = make(chan utils.Job, utils.MaxQueue)
}
func startQunue(name string, method interface{}, mqTp string, tp int, exhange string) {
	utils2.Log(nil, "qunue", name)
	if tp == 0 {
		go MqManager.GetMqByName(mqTp).Consume(name, method)
	} else {
		go rabbitmq.DelayConsume(name, exhange, method.(func(tag uint64, ch *amqp.Channel, msg []byte)))
	}

}

//func initOrderQunue() {
//	var orders, err = redis.GetRedis().HGetAll(utils2.GetRealKey(":") + "queryOrder")
//	if err == nil {
//		for _, v := range orders {
//			var notify = entityV1.MerchartNotify{}
//			json.Unmarshal([]byte(v), &notify)
//			fmt.Println("恢复", string(v))
//			err = MqManager.GetMqByName(RabbitMqQunue).Produce(PaymentQueryQueue, notify, 1, PaymentQueryExchange)
//		}
//		redis.GetRedis().HDel(utils.GetRealKey("queryOrder"))
//	}
//	orders, err = redis.GetRedis().HGetAll(utils.GetRealKey(":") + "notifyOrder")
//	if err == nil {
//		for _, v := range orders {
//			var notify = entityV1.MerchartNotify{}
//			json.Unmarshal([]byte(v), &notify)
//			fmt.Println("恢复", string(v))
//			err = MqManager.GetMqByName(RabbitMqQunue).Produce(PayMerchantNotify, notify, 0)
//		}
//		redis.GetRedis().HDel(utils.GetRealKey("queryOrder"))
//	}
//}
func StartQunueServer() error {
	if config.GetConf().StartQunue == 1 {
		//initOrderQunue()
		//startQunue(utils2.GetRealKey("_")+ACCOUNT_CHANGE, accountChange, RabbitMqQunue,0, "")                    //账变记录
		for i := 0; i < config.GetConf().QueryNum; i++ {
			//startQunue(utils.GetRealKey("_")+PayMerchantNotifyNow, payOutNotify, NsqMqQunue,0,"") //账变记录
			//startQunue(utils.GetRealKey("_")+PaymentQueryQueue, paymentQuery, NsqMqQunue,0, PaymentQueryExchange)            //批量消费
			//startQunue(utils.GetRealKey("_")+TrxQueryPayQunue, TrxQueryPay, NsqMqQunue,0, TrxQueryExchange)
			//startQunue(utils.GetRealKey("_")+TrxQueryPayoutQunue, TrxQueryPayout, NsqMqQunue,0, TrxQueryExchange)//trx回调
			//startQunue(utils.GetRealKey("_")+PayMerchantNotify, payOutNotify, NsqMqQunue,0, PayMerchantExchange) //商户通知
			//startQunue(utils.GetRealKey("_")+PAYOUT, payout, NsqMqQunue, 0,"")
			//startQunue(utils.GetRealKey("_")+TrxQueryPayQunue, TrxQueryPay, NsqMqQunue,0, TrxQueryExchange)
			//startQunue(utils.GetRealKey("_")+TrxOrderHandle,HandTrxOrder,NsqMqQunue,0,"")
			//startQunue(utils.GetRealKey("_")+SubAccountForzen,HandSubAccount,NsqMqQunue,0,"")
		}
	}
	startWorkers()
	select {}
	return nil
}
func startWorkers() {
	//创建dispatcher
	//dispatcher := utils.NewDispatcher(utils.MaxWorkers)
	//dispatcher.Run()
	//defer dispatcher.Stop()
	select {}
}
