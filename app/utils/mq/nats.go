package rabbitmq

import (
	"encoding/json"
	"github.com/nats-io/nats.go"
	"github.com/streadway/amqp"
	"snow-im/app/utils"
)

type NatsMq struct {
	address string
}
func (n NatsMq) Produce(name string, log interface{}, delayTime int, args ...interface{}) error {
	nc, _ := nats.Connect(n.address)
	var content,err = json.Marshal(log)
	nc.Publish(name, content)
	return err
}

func (n NatsMq) Comsume(name string, hand interface{}) *nats.Conn  {
	nc, _ := nats.Connect(n.address)
	nc.Subscribe(name, func(m *nats.Msg) {
		utils.Log(nil,"Received a message: %s", string(m.Data))
		var handler = hand.(func(tag uint64, ch *amqp.Channel, msg []byte))
		handler(0,nil,m.Data)
	})
	return nc
}