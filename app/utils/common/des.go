package common

import (
	"fmt"
	"github.com/forgoer/openssl"
)

func Des3Encrypt(src []byte,key string) ([]byte,error) {
	dst, err := openssl.Des3ECBEncrypt(src, []byte(key), openssl.PKCS7_PADDING)
	return dst,err
}

func Des3ECBDecrypt(data []byte,key string) ([]byte,error) {
	dst, err := openssl.Des3ECBDecrypt(data, []byte(key), openssl.PKCS7_PADDING)
	fmt.Println(string(dst))
	return  dst,err
}
