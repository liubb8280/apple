package httpclient

import (
	"fmt"
	"snow-im/app/utils"

	"github.com/valyala/fasthttp"
	"time"
)

func FastHttpPost(url string, header map[string]string, body []byte, timeout int) ([]byte, error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req) // 用完需要释放资源
	// 默认是application/x-www-form-urlencoded
	req.Header.SetMethod("POST")
	for k, v := range header {
		req.Header.Set(k, v)
	}
	req.SetRequestURI(url)
	req.SetBody(body)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp) // 用完需要释放资源
	var err error
	if timeout > 0 {
		if err = fasthttp.Do(req, resp); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	} else {
		if err := fasthttp.DoTimeout(req, resp, time.Duration(timeout)*time.Second); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	}
	b := resp.Body()
	//fmt.Println(string(b),"http请求")
	return b, nil
}

func FastHttpPostForm(url string, header map[string]string, body map[string]string, timeout int) ([]byte, error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req) // 用完需要释放资源
	// 默认是application/x-www-form-urlencoded
	req.Header.SetMethod("POST")
	for k, v := range header {
		req.Header.Set(k, v)
	}
	req.SetRequestURI(url)
	args := &fasthttp.Args{}
	for k, v := range body {
		args.Add(k, v)
	}
	req.SetBody(args.QueryString())
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp) // 用完需要释放资源
	var err error
	if timeout == 0 {
		if err = fasthttp.Do(req, resp); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	} else {
		if err := fasthttp.DoTimeout(req, resp, time.Duration(timeout)*time.Second); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	}
	b := resp.Body()
	return b, nil
}
func FastHttpGet(url string, header map[string]string, body map[string]string, timeout int) ([]byte, error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req) // 用完需要释放资源
	// 默认是application/x-www-form-urlencoded
	req.Header.SetMethod("GET")
	for k, v := range header {
		req.Header.Set(k, v)
	}
	if len(body) > 0 {
		url += "?"
		for k, v := range body {
			url += k + "=" + v + "&"
		}
		url = url[0 : len(url)-1]
	}
	fmt.Println(url)
	req.SetRequestURI(url)
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp) // 用完需要释放资源
	var err error
	if timeout == 0 {
		if err = fasthttp.Do(req, resp); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	} else {
		if err := fasthttp.DoTimeout(req, resp, time.Duration(timeout)*time.Second); err != nil {
			utils.Log(nil, "http请求失败", err, url)
			return nil, err
		}
	}
	b := resp.Body()
	return b, nil
}
