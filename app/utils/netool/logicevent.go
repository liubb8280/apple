package netool

import (
	"github.com/go-netty/go-netty"
	_ "github.com/go-netty/go-netty-transport/websocket"
	_ "github.com/go-netty/go-netty/codec/format"
	_ "github.com/go-netty/go-netty/codec/frame"
	utils2 "github.com/go-netty/go-netty/utils"
	"snow-im/app/utils"
)

type LogicHandler struct {
	Handler MsgHandler
}

func (l LogicHandler) HandleActive(ctx netty.ActiveContext) {
	utils.Log(nil, "connect", ctx.Channel())
	GetConnManagger().SaveConnId(ctx.Channel().ID(), ctx.Channel())
	ctx.HandleActive()
}

func (l LogicHandler) HandleRead(ctx netty.InboundContext, message netty.Message) {
	var msgByte = utils2.MustToBytes(message)
	err := l.Handler.HandMessage(msgByte, ctx)
	utils2.AssertIf(err != nil, err.Error())
}

func (l LogicHandler) HandleInactive(ctx netty.InactiveContext, ex netty.Exception) {
	utils.Log(nil, "dis connect")
	GetConnManagger().DelConnection(ctx.Channel().ID())
	ctx.HandleInactive(ex)
}
