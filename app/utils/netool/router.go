package netool

import (
	"errors"
	"github.com/go-netty/go-netty"
	"snow-im/app/constants/errorcode"
)

type Router struct {
	routes map[int]func(msg []byte, connect netty.InboundContext) error
}

func (this *Router) AddRouter(msgId int, handler func(msg []byte,  connect netty.InboundContext) error) {
	if this.routes == nil {
		this.routes = make(map[int]func(msg []byte, connect netty.InboundContext) error)
	}
	this.routes[msgId] = handler
}

func (this *Router) HandleMsg(msgId int, msg []byte, connect netty.InboundContext) error {
	var route = this.routes[msgId]
	if route != nil{
		return route(msg, connect)
	}else {
		return errors.New(errorcode.GetMsg(errorcode.NoRoute,""))
	}

}
