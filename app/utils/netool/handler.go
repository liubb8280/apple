package netool

import (
	"github.com/go-netty/go-netty"
)

type MsgHandler interface {
	//消息处理
	HandMessage([]byte,  netty.InboundContext) error
}

