package ordermodel

import (
	"github.com/qit-team/snow-core/db"
	"sync"
)

var (
	once sync.Once
	m    *OrderModel
)

// 实体
type Order struct {
	Id        int    `json:"id" xorm:"'id' int(10) pk autoincr"`
	Uuid      string `json:"uuid" xorm:"'uuid' varchar(120)"`
	TransId   string `json:"trans_id" xorm:"'trans_id' varchar(120)"`
	OrginId   string `json:"origin_id" xorm:"'origin_id' varchar(120)"`
	Recipe    string `json:"-" xorm:"'recipe' TEXT" `
	Expire    int64  `json:"expire" xorm:"'expire' int(10)"`
	GoodsId   string `json:"goods_id" xorm:"'goods_id' varchar(120)"`
	CreatedAt int64  `json:"created_at" xorm:"'created_at' int(10)"`
	Status    int    `json:"status" xorm:"'status' int(10)"`
}

type OrderDetail struct {
	Order     `json:"order"`
	GoodsName string `json:"goods_name"`
}

// 表名
func (m *Order) TableName() string {
	return "order"
}

// 私有化，防止被外部new
type OrderModel struct {
	db.Model //组合基础Model，集成基础Model的属性和方法
}

// 单例模式
func GetInstance() *OrderModel {
	once.Do(func() {
		m = new(OrderModel)
		//m.DiName = "" //设置数据库实例连接，默认db.SingletonMain
	})
	return m
}
