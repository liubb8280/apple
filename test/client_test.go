package test

import (
	"encoding/binary"
	"fmt"
	"github.com/go-netty/go-netty"
	"github.com/go-netty/go-netty/codec/frame"
	"github.com/go-netty/go-netty/utils"
	ws "golang.org/x/net/websocket"
	utils2 "snow-im/app/utils"
	"testing"
	"time"
)

type EchoHandler struct {
	role string
}

func (l EchoHandler) HandleActive(ctx netty.ActiveContext) {
	fmt.Println(l.role, "->", "active:", ctx.Channel().RemoteAddr())

	//ctx.Write("Hello I'm " + l.role)
	ctx.HandleActive()
}

func (l EchoHandler) HandleRead(ctx netty.InboundContext, message netty.Message) {
	fmt.Println(l.role, "->", "handle read:", string(utils.MustToBytes(message)))
	ctx.HandleRead(message)
}

func (l EchoHandler) HandleInactive(ctx netty.InactiveContext, ex netty.Exception) {
	utils2.Log(nil,l.role, "->", "inactive:", ctx.Channel().RemoteAddr(), ex)
	ctx.HandleInactive(ex)
}

func Test_Websocket(t *testing.T) {
	var wsurl = "ws://127.0.0.1:8071/chat"
	var origin = "http://127.0.0.1:8071/"
	ws, err := ws.Dial(wsurl, "", origin)
	if err != nil {
		panic(err)
	}
	var body = utils2.PackMsg([]byte("hello word中国"), 1000)
	var data = make([]byte, 0)
	var head = utils2.Int16ToBytes(int16(len(body)))
	data = append(data, head...)
	data = append(data, body...)
	rs, err := ws.Write(data)
	fmt.Println(rs, err,data)
	select {

	}
}

func Test_Client(t *testing.T) {
	clientInitializer := func(channel netty.Channel) {
		channel.Pipeline().
			AddLast(frame.LengthFieldCodec(binary.LittleEndian, 102400, 0, 2, 0, 2)).
			//AddLast(format.TextCodec()).
			AddLast(EchoHandler{})
	}

	// new bootstrap
	var bootstrap = netty.NewBootstrap(netty.WithClientInitializer(clientInitializer))
	// connect to the server after 1 second
	conn, err := bootstrap.Connect("127.0.0.1:8070", nil)
	defer conn.Close()
	fmt.Println(err)
	time.AfterFunc(time.Second, func() {
		time.Sleep(1 * time.Second)
		if err == nil {

			conn.Write(utils2.PackMsg([]byte("hello"), 1000))

		}

		//utils.Assert(err)
	})

	select {

	}

	// setup bootstrap & startup server.
	//bootstrap.Listen("0.0.0.0:6565",kcp.WithOptions(kcp.DefaultOptions)).Sync()
}
